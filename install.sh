#!/bin/bash

if [[ $EUID -ne 0 ]]; then
    echo "Must be run with sudo"
    exit 1
fi

sudo apt install -y python2.7

# Make sure coolkey is installed
COOLKEY_LOC="/usr/lib64/libcackey.so"
VMWARE_COOLKEY_LOC="/usr/lib/vmware/view/pkcs11/libcoolkeypk11.so"
if test -f "$COOLKEY_LOC"; then
    echo "cackey already installed"
else
    sudo apt install -y \
    pcsc-tools \
    pcscd 
    mkdir -p cackey
    cd cackey
    rm cackey*
    wget http://cackey.rkeene.org/download/0.7.5/cackey-0.7.5-x86_64-1.tgz
    tar -xf cackey*
    mkdir -p /usr/lib64/
    cp usr/lib64/libcackey* /usr/lib64/
    cd ..
fi

if hash vmware-view 2>/dev/null; then
    echo "VMWare already installed"
else
    # Install VMWare Horizons silently
    if ! test -f horizon-5.3.0.bundle; then
    	wget -O horizon-5.3.0.bundle https://download3.vmware.com/software/view/viewclients/CART20FQ4/VMware-Horizon-Client-5.3.0-15208949.x64.bundle
    fi
    sudo env TERM=dumb VMWARE_EULAS_AGREED=yes \
    ./horizon-5.3.0.bundle  --console --required \
    --set-setting vmware-horizon-smartcardsmartcardEnable yes \
    --set-setting vmware-horizon-rtavrtavEnable yes \
    --set-setting vmware-horizon-virtual-printing tpEnable yes \
    --set-setting vmware-horizon-tsdrtsdrEnable yes \
    --set-setting vmware-horizon-mmr mmrEnableyes \
    --set-setting vmware-horizon-media-provider mediaproviderEnable yes
fi

rm "$VMWARE_COOLKEY_LOC"
echo "Symlinking cackey"
mkdir -p "/usr/lib/vmware/view/pkcs11/"
sudo ln -s "$COOLKEY_LOC" "$VMWARE_COOLKEY_LOC"

echo "Adding certs"
sudo cp PEM_certs/* /usr/local/share/ca-certificates/
sudo update-ca-certificates

echo "Adding TLS 1.1 support"
mkdir -p ~/.vmware/
chown -R $SUDO_USER:$SUDO_USER ~/.vmware/
echo "view.sslProtocolString = \"TLSv1.1\"" >> ~/.vmware/view-preferences
chown -R $SUDO_USER:$SUDO_USER ~/.vmware/view-preferences

echo "Installation complete! Launch Horizons and add the following server:"
echo "VDI Address: https://afrcdesktops.us.af.mil"